import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Network } from '@ionic-native/network';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    private netWork: Network) {
    platform.ready().then(() => {

      statusBar.styleDefault();
      statusBar.backgroundColorByHexString("#fff");
      splashScreen.hide();

  

    this.netWork.onConnect().subscribe(()=>{
      

    });
  });
  }
}

