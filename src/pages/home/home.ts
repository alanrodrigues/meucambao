import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { HorariosProvider } from '../../providers/servico/horarios';
import { HorariosSaida } from '../../model/HorariosSaida';
import { HorarioRua } from '../../model/HorarioRua';
import { Linhas } from '../../model/linhas';
import { LinhasProvider } from '../../providers/servico/linhas';
import { DatePicker } from '@ionic-native/date-picker';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public horariosSaida:HorariosSaida;
  public horariosRua: HorarioRua;
  public linha: Linhas;
  public resultado;
  public dataHoje;
  items: string[];

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    public linhas: LinhasProvider,
    private datePicker: DatePicker,
    public horarios: HorariosProvider) {
    
      
  }

  async teste(ev){
    this.resultado = await this.linhas.getLinha();
    const val = ev.target.value;
    if (val && val.trim() != '') {
      this.resultado =  this.resultado.filter((item) => {
        return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
        
      })
    }
    
  }

  async pesquisarOnibus(onibus) {
    if (onibus != null) {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
    }).then(async (date) => {
        let dia: any;
        dia = date.getDate() > 10 ? date.getDate() : '0' + date.getDate();
        this.dataHoje = date.getFullYear() + '0' + (date.getMonth() + 1) + '' + dia;
        let resultado = await this.horarios.getHorarios(onibus.numero, this.dataHoje);
        this.horariosSaida = resultado[0];
        this.horariosRua = resultado[1];
      }).catch((err) =>{
        this.mostrarErro(err);
      });
    } else {
      let toast = this.toastCtrl.create({
        message: 'Preencha o campo de linha de ônibus',
        duration: 3000,
        position: 'bottom'
      });
  
      toast.present();
    }
  }

  mostrarErro(erro){
    let toast = this.toastCtrl.create({
      message: erro,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.present();
  }


}
