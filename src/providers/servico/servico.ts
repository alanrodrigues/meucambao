import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ToastController } from 'ionic-angular';

interface RetornoWebService {
  retorno: any,
  exception?: {
    errorCode: number,
    errorMessage: string
  }
};

@Injectable()
export class ServicoProvider {
  constructor(public http: Http,
    private toastCtrl: ToastController) {

  }

  public async get<T>(body: number, data: any) {
    const response = await this.http.get('http://gistapis.etufor.ce.gov.br:8081/api/horarios/' + body + '?data=' + data).toPromise();

    
    const retornoWebService: RetornoWebService = response.json();
    

    if (retornoWebService.exception != null) {
      throw new Error(retornoWebService.exception.errorMessage);
    }

    return retornoWebService;
  }

  public async getLinhas() {
    const response = await this.http.get('http://gistapis.etufor.ce.gov.br:8081/api/linhas/').toPromise();
    const retornoWebService: RetornoWebService = response.json();

    return retornoWebService;
  }

}
