import { Injectable } from '@angular/core';
import { ServicoProvider } from './servico';
import { HorariosSaida } from '../../model/HorariosSaida';


@Injectable()
export class HorariosProvider extends ServicoProvider {
  public getHorarios(numeroLinha, data){
    return this.get<HorariosSaida>(numeroLinha, data);
  }
}
