import { Injectable } from '@angular/core';
import { ServicoProvider } from './servico';

/*
  Generated class for the LinhasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LinhasProvider extends ServicoProvider{
  public getLinha(){
    return this.getLinhas();
  }
}
