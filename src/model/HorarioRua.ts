import { Horario } from "./Horario";

export class HorarioRua{
    public postoControle: String;
    public horarios: Horario[];
}