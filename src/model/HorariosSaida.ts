import { Horario } from "./Horario";

export class HorariosSaida{
    public postoControle: String;
    public horarios: Horario[];
}